package shino;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

public class OrderManager {

    private static SessionFactory FACTORY;

    public static void main( String[] args) {
        try {
            FACTORY = new Configuration().configure().buildSessionFactory();
        } catch (Throwable ex) {
            System.err.println("Failed to create sessionFactory object: " + ex);
            throw new ExceptionInInitializerError(ex);
        }
        OrderManager manager = new OrderManager();

        /* Add few order records in database */
        Integer order1 = manager.addOrder("One", 1);
        Integer order2 = manager.addOrder("Two", 2);
        Integer order3 = manager.addOrder("Three", 1);
        manager.updateOrderName(order1, "One_modified");
        manager.listOrders();
        manager.deleteOrder(order1);
        manager.deleteOrder(order2);
        manager.deleteOrder(order3);
    }

    public Integer addOrder(String name, int accountId) {
        Integer orderId = runWithTransaction(session -> {
            Order order = new Order(name, accountId);
            return (Integer) session.save(order);
        });
        System.out.println("order ID: " + orderId + " successfully added.");
        return orderId;
    }

    public void listOrders() {
        runWithTransaction(session -> {
            List<Order> orders = (List<Order>) session.createQuery("FROM Order").list();
            for (Order order : orders) {
                System.out.println("Order ID: " + order.getId());
                System.out.println("Order name: " + order.getName());
                System.out.println("Account ID: " + order.getAccount());
                System.out.println("====================");
            }
            return orders;
        });
    }

    public void updateOrderName(Integer orderId, String name) {
        runWithTransaction(session -> {
            Order order = session.get(Order.class, orderId);
            order.setName(name);
            session.update(order);
            return orderId;
        });
    }

    public void deleteOrder(Integer orderId) {
        runWithTransaction(session -> {
            Order order = session.get(Order.class, orderId);
            session.delete(order);
            System.out.println("Order ID " + orderId + " deleted");
            return orderId;
        });
    }

    // lambda test
    public <T> T runWithTransaction(Function<Session, T> function) {
        Session session = FACTORY.openSession();
        Transaction tx = null;
        T result = null;
        try {
            tx = session.beginTransaction();
            result = function.apply(session);
            tx.commit();
            return result;
        } catch (HibernateException ex) {
            if (tx != null) tx.rollback();
            ex.printStackTrace();
            throw new RuntimeException(ex);
        } finally {
            session.close();
            System.out.println("-----------------------------------");
        }
    }
}
