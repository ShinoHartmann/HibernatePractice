package shino;

import javax.persistence.*;
import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "product_order", uniqueConstraints = {
        @UniqueConstraint(columnNames = "id") })
public class Order {
    private int id;
    private String name;
    private int account;

    public Order() {}

    public Order(String name, int account) {
        this.name = name;
        this.account = account;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "name", unique = false, nullable = false, length = 45)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "account", unique = false, nullable = false)
    public int getAccount() {
        return account;
    }

    public void setAccount(int account) {
        this.account = account;
    }
}
